let totalDocuments, totalProcessed = 0;

describe('Check environment', () => {

    it('Visits the converter page', () => {
        cy.visit('http://localhost:3002')
    })

    it('Collapse CKEditor inspector if expanded', () => {
        cy.get('.ck-inspector-navbox__navigation__toggle').click();
    })
})

describe('Prepare expectations', function () {

    it('should have items to process', function () {
        cy.get('#totalDocuments').contains(/\d+/);
    });

    it('should have items to process setter', function () {
        cy.get('#totalDocuments').then(($span) => {
            totalDocuments = parseInt($span.text());
        })
    });

    it('should be the same number of items to be clicked', () => {
        cy.get('.MuiTableBody-root')
         .find('.MuiTableRow-root')
         .its('length').should('eq', totalDocuments);
    })
});

describe('Convert documents', function () {
    it('should convert all documents', function () {
        cy.get('.MuiTableBody-root')
            .find('tr.MuiTableRow-root>td.MuiTableCell-root>.MuiButtonBase-root')
            .each( ($el, index, $list) => {
                cy.wrap($el).click().then(() => {
                    totalProcessed = 1 + totalProcessed
                    cy.wait(500);
                });
            });
    });

    it('should have processed all documents', function () {
        expect(totalDocuments).to.equal(totalProcessed);
    });

});
