import React from 'react';
import './App.css';
import {withStyles} from '@material-ui/core/styles';
import {AppBar, Grid, Toolbar, Typography} from '@material-ui/core';
import HtmlEditor from "./components/HtmlEditor";
import HtmlViewer from "./components/HtmlViewer";
import SimpleTable from './components/SimpleTable';
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from '@material-ui/icons/Menu';
import Console from "./components/Console";

const styles = theme => ({

    '@global': {
        body: {
            backgroundColor: theme.palette.common.white,
        },
    },
    root: {
        flexGrow: 1,
    },
    paper: {
        margin: theme.spacing(1),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
});

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: 'App',
            documentId: null,
            error: null,
            isLoaded: false,
        };
    }

    consoleEl = React.createRef();
    htmlEditorEl = React.createRef();
    htmlViewerEl = React.createRef();
    simpleTableEl = React.createRef();

    logger = (v) => {
        this.consoleEl.current.appendToTerminal(v);
    }

    onFoo = (v) => {
        this.logger(`action: 'handleData', params: '${v}', status: 'started'`);

        this.setState({
            editorData: v,
        })
        const url = `${process.env.REACT_APP_API_URL}/section?${v}`;
        fetch(url)
            .then(res => res.json())
            .then(
                (result) => {
                    this.logger(`action: 'handleData', params: '${v}', status: 'in-progress'`);
                    this.setState({
                        name: 'SimpleTableItemStatus',
                        documentId: v,
                        error:null,
                        isLoaded: true
                    });
                    this.htmlEditorEl.current.handleData(result, v);
                    this.htmlViewerEl.current.handleData(result);
                },
                (error) => {
                    this.logger(`action: 'onFoo.fetch', params: '${v}', status: 'error', msg: '${error}'`);
                    this.setState({
                        name: 'SimpleTableItemStatus',
                        documentId: v,
                        error,
                        isLoaded: true
                    });
                }
            )
    }

    documentWasConverted = (k) =>  {
        this.simpleTableEl.current.toggleConvertedLabel(k);
    }

    render() {
        const {classes} = this.props;
        return (
            <React.Fragment>

                <AppBar position="static">
                    <Toolbar>
                        <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                            <MenuIcon/>
                        </IconButton>
                        <Typography variant="h6"
                                    className={classes.title}>
                            Consent Admin UI
                        </Typography>
                        <Button color="inherit">convert</Button>
                    </Toolbar>
                </AppBar>

                <Grid container className={classes.grid}
                      direction="row"
                      justify="flex-start"
                      alignItems="stretch">

                    <Grid item xs={3}>
                        <SimpleTable ref={this.simpleTableEl}
                                     foo={this.onFoo}/>
                    </Grid>

                    <Grid item xs={9}>

                        <Grid container className={classes.grid}
                              direction="row"
                              justify="flex-start"
                              alignItems="stretch">
                            <Grid item xs={6} style={{padding:10, backgroundColor:'#f0f0f0'}}>
                                <HtmlEditor ref={this.htmlEditorEl}
                                            documentId={this.state.documentId}
                                            documentWasConverted={this.documentWasConverted}
                                            logger={this.logger}
                                            />
                            </Grid>
                            <Grid item xs={6} style={{padding:10, backgroundColor:'#f0f0f0'}}>
                                <HtmlViewer ref={this.htmlViewerEl}
                                />
                            </Grid>
                        </Grid>
                    </Grid>

                </Grid>


                <Grid item xs={12} style={{padding:10, backgroundColor:'#f0f0f0'}}>
                    <Console ref={this.consoleEl} />
                </Grid>


            </React.Fragment>
        );
    }
}

export default withStyles(styles)(App);
