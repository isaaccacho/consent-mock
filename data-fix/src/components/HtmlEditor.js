import React, { Component } from 'react';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-editor-classic/src/classiceditor';
import CKEditorInspector from '@ckeditor/ckeditor5-inspector';
import { editorConfiguration } from './EditorDefaultConfig'
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";

let ckeditor;

class HtmlEditor extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: 'HtmlEditor',
            error: null,
            isLoaded: false,
        };
    }

    handleData = (v, i) => {
        ckeditor.setData(`transforming '${i}' ...`);

        const { id, wording, altId, type, section, status, state, name, converted } = v[0];

        this.setState({
            id, wording, altId, type, section, status, state, name, converted
        });

        try {
            ckeditor.setData(wording);
            if (!converted) {
                this.props.logger(`action: 'handleData', id: '${id}', status: 'model-converted'`);
                this.putData(id, altId, type, section, status, state, name);
            } else {
                this.props.logger(`action: 'handleData', id: '${id}', status: 'already-converted'`);
            }
        } catch (error) {
            this.props.logger(`action: 'putData', id: '${id}', status: 'update-failed' ❌, msg: '${error.message}'`);
        }
    }

    handleUpdate = () => {
        const { id, altId, type, section, status, state, name } = this.state;

        this.props.logger(`action: 'handleUpdate', id: '${id}', status: 'updating'`);

        this.putData(id, altId, type, section, status, state, name);
    }

    putData = (id, altId, type, section, status, state, name, v) => {
        this.props.logger(
            `action: 'putData', id: '${id}', status: 'update-pending'`
        );

        const data = {
            id,
            altId,
            type,
            section,
            state,
            name,
            wording: ckeditor.getData(),
            status,
            version: 0,
            addedBy: '',
            converted: true
        };

        fetch(`${process.env.REACT_APP_API_URL}/section/${data.id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        })
            .then(response => response.json())
            .then(fdata => {
                this.props.logger(`action: 'putData', id: '${id}', status: 'update-succeed' ✅`);
                const { wording } = fdata;

                try {
                    ckeditor.setData(wording);
                    this.props.documentWasConverted(id);
                } catch (e) {
                    console.error('Error:', e.message);
                    this.props.logger(`action: 'putData.setData', id: '${id}', status: 'setData-failed' ❌`);
                }
            })
            .catch((e) => {
                console.error('Error:', e.message);
                this.props.logger(`action: 'putData', id: '${id}', status: 'update-failed' ❌`);
            });

    }

    componentDidMount() {
        this.setState({
            isLoaded: true,
        });
    }

    render() {
        const { error, isLoaded } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <Paper style={{padding: 5}}>
                    Document: <strong>{this.state.name}</strong>, Id: {this.state.id}
                    <CKEditor
                        config={editorConfiguration}
                        editor={ClassicEditor}
                        onInit={editor => {
                            ckeditor = editor;
                            CKEditorInspector.attach(ckeditor);
                        }}
                    />
                    <Button variant="contained" color="primary"
                            onClick={this.handleUpdate}
                            style={{marginTop: 5}}>
                        Save
                    </Button>
                </Paper>
            );
        }
    }
}

export default HtmlEditor;
