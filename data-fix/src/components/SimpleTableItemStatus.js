import Typography from "@material-ui/core/Typography";
import React from "react";

class SimpleTableItemStatus extends React.Component {

    state = {
        name: 'SimpleTableItemStatus',
        error: null,
        isConverted: false,
        isLoaded: false,
        itemId: null,
    }

    constructor(props) {
        super(props);

        this.toggleConvertedState = this.toggleConvertedState.bind(this);
    }

    componentDidMount() {
        this.setState({
            name: 'SimpleTableItemStatus',
            isLoaded: true,
            itemId: this.props.itemId,
            isConverted: this.props.isConverted,
        });
    }

    toggleConvertedState = (k) => {
        console.log('toggleConvertedState: '+k)
        this.setState({
            itemId: k,
            isConverted: true
        })
    }

    render(){
        const { error, isLoaded, isConverted } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return  (
                isConverted
                    ?
                    <Typography
                        style={{color: 'green'}} display="inline" gutterBottom>
                        ✓
                    </Typography>
                    :
                    <Typography
                        color="secondary" display="inline" gutterBottom>
                        ✕
                    </Typography>

            )
        }
    }

}

export default SimpleTableItemStatus;
