import React from "react";
import Tab from '@material-ui/core/Tab';
import moment from "moment";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const {Component} = require("react");

class HtmlViewer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: 'HtmlViewer',
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        this.setState({
            isLoaded: true,
        });
    }

    handleData = (items) => {
        this.setState({
            items
        })
    }

    getTabItems = () => {
        return (
            <Tab label={'hi'} />
        );
    }

    useStyles = makeStyles((theme) => ({
        root: {
            width: '100%',
        },
        heading: {
            fontSize: theme.typography.pxToRem(15),
            fontWeight: theme.typography.fontWeightRegular,
        },
    }));

    render() {
        const { error, isLoaded, items } = this.state;
        const allButCurrent = items
            .filter((item) => item.version > 0)
            .sort((a, b) => b.version - a.version);
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            const classes = this.useStyles;
            return (
                <div className={classes.root}>
                    {allButCurrent.map((item, index) => {
                        return (
                            <Accordion key={`p-hv-${index}`}>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1a-content"
                                    id="panel1a-header"
                                >
                                    <Typography className={classes.heading}>
                                        Version: <strong>{moment(item.version).format("YY/MM/DD, HH:mm:ss A")}</strong>
                                    </Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <div dangerouslySetInnerHTML={{__html: item.wording}}
                                         style={{maxHeight: 800, overflowY: "auto"}}/>
                                </AccordionDetails>
                            </Accordion>
                        );
                    })}
                </div>
            );
        }


    }

}

export default HtmlViewer;
