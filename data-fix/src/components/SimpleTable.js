import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import SimpleTableAction from "./SimpleTableAction";
import SimpleTableItemStatus from "./SimpleTableItemStatus";
import withStyles from "@material-ui/core/styles/withStyles";

const useStyles = {
    root: {
        width: '100%',
    },
    table: {
        minWidth: 400,
    },
    container: {
        maxHeight: 800,
    },
}

class SimpleTable extends React.Component {

    simpleTableItemStatusEl = React.createRef();
    itemRefs = [];

    constructor(props) {
        super(props);
        this.state = {
            name: 'SimpleTable',
            error: null,
            isLoaded: false,
            rows: [],
            url: `${process.env.REACT_APP_API_URL}/section?alt=0&type=${process.env.REACT_APP_DOC_TYPE}`,
        };
        this.toggleConvertedLabel = this.toggleConvertedLabel.bind(this);
    }

    componentDidMount() {
        fetch(this.state.url)
            .then(res => res.json())
            .then(
                (result) => {

                    this.setState({
                        isLoaded: true,
                        rows: result,
                    });

                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    toggleConvertedLabel = (k) => {
        console.log(`calling toggleConvertedLabel.id: 'sti-status-${k}'`);
        this[`sti-status-${k}`].toggleConvertedState(k);
    }

    setRef = (ref) => {
        this.itemRefs.push(ref);
    };

    render() {
        const { error, isLoaded, rows } = this.state;
        const { classes } = this.props;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            console.debug(`component: '${this.state.name}', rows: '${rows.length}', status: 'mounted'`);

            return (
                <Paper>
                    <TableContainer className={classes.container}>
                        <Table stickyHeader
                               aria-label="sticky table"
                               className={classes.table}
                               size="small">
                            <TableHead>
                                <TableRow>
                                    <TableCell></TableCell>
                                    <TableCell>Id</TableCell>
                                    <TableCell>Status</TableCell>
                                    <TableCell>Documents (<span id='totalDocuments'>{rows.length}</span>)</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {rows.map((row, index) => {
                                    const { altId, converted, id, name, section, state, type } = row;
                                    return (
                                        <TableRow key={`${id}`} id={id}>
                                            <TableCell>
                                                <SimpleTableAction
                                                    action={this.props.foo}
                                                    id={`alt=${altId}&type=${type}&section=${section}&state=${state}`}/>
                                            </TableCell>
                                            <TableCell>
                                                {id}
                                            </TableCell>
                                            <TableCell>
                                                <SimpleTableItemStatus
                                                    key={`sti-status-${id}`}
                                                    isConverted={converted}
                                                    itemId={id}
                                                    ref={(ref)=>{this[`sti-status-${id}`]=ref}}
                                                />
                                            </TableCell>
                                            <TableCell scope="row">
                                                {name}
                                            </TableCell>
                                        </TableRow>
                                    )}
                                )}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Paper>
            );
        }
    }
}

export default withStyles(useStyles)(SimpleTable);
