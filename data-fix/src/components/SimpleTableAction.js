import React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';

class SimpleTableAction extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: 'SimpleTableAction',
            error: null,
            isLoaded: false,
        };

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        const {action, id } = this.props;
        action(id);
    }

    useStyles = makeStyles((theme) => ({
        margin: {
            margin: theme.spacing(1),
        },
        extendedIcon: {
            marginRight: theme.spacing(1),
        },
    }))

    componentDidMount() {
        console.debug(`component: '${this.state.name}', status: 'mounted'`);
        this.setState({
            isLoaded: true,
        });
    }

    render() {
        const { error, isLoaded } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <IconButton size="small" className={this.useStyles.margin}
                            onClick={this.handleClick}>
                    <EditIcon  />
                </IconButton>
            );
        }
    }

}

export default SimpleTableAction;
