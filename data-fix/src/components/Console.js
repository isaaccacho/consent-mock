import {Paper, Typography} from "@material-ui/core";
import React from "react";

class Console extends React.Component {

    consoleEl = React.createRef();

    state = {
        name: 'Console',
        consoleData: 'ready ...',
    }

    componentDidMount() {
        this.appendToTerminal(`component: '${this.state.name}', status: 'mounted'`);
    }

    appendToTerminal = val => {
        const msg = `${new Date().toISOString()} - ${val}`;
        console.log(msg);
        /*
        this.consoleEl.current.appendChild(
            document.createTextNode(`\n${new Date().toISOString()} - ${val}`)
        )
        */
    }

    render() {
        return (
            <Paper style={{margin: 10, padding: 10}}>
                <Typography
                    color="primary"
                    variant="caption" display="block" gutterBottom>
                    console
                </Typography>
                <pre style={{fontSize: 11, backgroundColor: '#f2f2f2', padding: 10}}>
                    <code ref={this.consoleEl}/>
                </pre>
            </Paper>);
    }
}

export default Console;
